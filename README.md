# Fuzzing Setup

This is my documented efforts in trying to fuzz
[libvirt](https://libvirt.org).

## Basic Setup

I am using a debian12 virtual machine to try fuzz libvirt. On a fresh
installation, you will need the following packages installed on your
system.

- Libvirt Dependencies
  ([source](https://gitlab.com/libvirt/libvirt/-/blob/master/ci/containers/debian-12.Dockerfile))

``` {.sh}
sudo apt install \
    augeas-lenses \
    augeas-tools \
    bash-completion \
    black \
    ca-certificates \
    ccache \
    clang \
    codespell \
    cpp \
    diffutils \
    dnsmasq \
    dwarves \
    ebtables \
    flake8 \
    gcc \
    gettext \
    git \
    grep \
    iproute2 \
    iptables \
    kmod \
    libacl1-dev \
    libapparmor-dev \
    libattr1-dev \
    libaudit-dev \
    libblkid-dev \
    libc6-dev \
    libcap-ng-dev \
    libclang-rt-dev \
    libcurl4-gnutls-dev \
    libdevmapper-dev \
    libfuse-dev \
    libglib2.0-dev \
    libglusterfs-dev \
    libgnutls28-dev \
    libiscsi-dev \
    libnbd-dev \
    libnl-3-dev \
    libnl-route-3-dev \
    libnuma-dev \
    libparted-dev \
    libpcap0.8-dev \
    libpciaccess-dev \
    librbd-dev \
    libreadline-dev \
    libsanlock-dev \
    libsasl2-dev \
    libselinux1-dev \
    libssh-gcrypt-dev \
    libssh2-1-dev \
    libtirpc-dev \
    libudev-dev \
    libxen-dev \
    libxml2-dev \
    libxml2-utils \
    libyajl-dev \
    locales \
    lvm2 \
    make \
    meson \
    nfs-common \
    ninja-build \
    numad \
    open-iscsi \
    perl-base \
    pkgconf \
    policykit-1 \
    python3 \
    python3-docutils \
    python3-pytest \
    qemu-utils \
    sed \
    systemtap-sdt-dev \
    wireshark-dev \
    xsltproc
```

- AFLPlusPlus Dependencies
  ([source](https://github.com/AFLplusplus/AFLplusplus/blob/stable/docs/INSTALL.md))

``` {.sh}
sudo apt-get install \
    build-essential \
    python3-dev \
    automake \
    cmake \
    git \
    flex \
    bison \
    libglib2.0-dev \
    libpixman-1-dev \
    python3-setuptools \
    cargo \
    libgtk-3-dev \
    lld \
    llvm \
    llvm-dev \
    clang \
    ninja-build 

sudo apt-get install \
    gcc-$(gcc --version|head -n1|sed 's/\..*//'|sed 's/.* //')-plugin-dev \
    libstdc++-$(gcc --version|head -n1|sed 's/\..*//'|sed 's/.* //')-dev 
```

(seperated the commands because the gcc-plugin-dev command requries gcc
to be installed.)

- By default the debian path doesn't contain `/sbin` and `/usr/sbin`,
  its good to update the path to have them

``` {.sh}
export PATH=/sbin:/usr/sbin/:/usr/local/sbin:$PATH
```
You have to put this in your shell config for it to be persistent.

- Even though I plan to fuzz mostly network and storage config, it is
  good to have qemu-kvm hypervisor installed.

``` {.sh}
sudo apt install qemu-kvm
```

## Building AFLPlusPlus

I will build all the modules using the `distrib` target. Even there
might be less optimizations but since we have to have a detailed
analysis about our fuzzing attempts and study the progress, its good to
have all these enabled.

``` {.sh}
git clone https://github.com/AFLplusplus/AFLplusplus
cd AFLplusplus
make PERFORMANCE=1 CODE_COVERAGE=1 PROFILING=1 INTROSPECTION=1 LLVM_CONFIG=llvm-config-14 distrib 
sudo make install
```

### Setting up libvirt and qemu

Now clone the libvirt main repo.

``` {.sh}
git clone https://gitlab.com/libvirt/libvirt.git
```

(build instructions change per fuzzing method)

- Add user to kvm group and libvirt group

``` {.sh}
sudo usermod -a -G kvm $USER
sudo usermod -a -G libvirt $USER
```

### Coverage

For coverage I am using `afl-cov`, and u need the following
dependencies:

``` {.sh}
sudo apt install lcov
```

- Get the alf-cov executable from github.

``` {.sh}
cd ~
git clone https://github.com/vanhauser-thc/afl-cov
cd afl-cov
sudo make install
```

This time while building libvirt (spare copy of original) add the flags
`-fprofile-arcs -ftest-coverage`.
