#!/bin/bash

export LD_LIBRARY_PATH="~/libvirt/build/src/"
export AFL_PRELOAD="$(ls ~/libvirt/build/src/*.so | tr -s '[:space:]' ' ')"
export PATH=$PATH:/usr/sbin

sudo systemctl start virtqemud.socket
sudo systemctl start virtqemud-admin.socket
sudo systemctl start virtqemud-ro.socket
sudo systemctl start virtnetworkd.socket
sudo systemctl start virtnetworkd-admin.socket
sudo systemctl start virtnetworkd-ro.socket

pkill virtqemud
pkill virtnetworkd
LD_PRELOAD="$(ls ~/libvirt/build/src/*.so | tr -s '[:space:]' ' ')" ~/libvirt/build/src/virtqemud -d 
LD_PRELOAD="$(ls ~/libvirt/build/src/*.so | tr -s '[:space:]' ' ')" ~/libvirt/build/src/virtnetworkd -d
