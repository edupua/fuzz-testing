#!/bin/bash

export SOURCE_CODE="~/libvirt-cov/build/src"
export LD_LIBRARY_PATH="~/libvirt-cov/build/src/"
export LD_PRELOAD="$(ls ~/libvirt-cov/build/src/*.so | tr -s '[:space:]' ' ')" 
