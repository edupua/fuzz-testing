# Fuzzing virsh

Before diving into the source code and trying to instrument specific
functions, we can try fuzzing the `virsh` binary. Though this might not
be the most efficient method, it is by far the most straighforward
approach.

Binaries are AFLPlusPlus's strength and that's what it was built for. So
lets compile libvirt with `afl-clang-fast` and test this out.

## Mutations

- I believe in XML-aware mutations in fuzzing. AFLPlusPlus has long
  supported custom mutators, and I have seen custom mutators be
  mentioned everywhere, but funny thing, barely anyone has actually used
  a mutator except the example mutators that [AFL
  provides](https://github.com/AFLplusplus/AFLplusplus/tree/stable/custom_mutators/examples).

- There's another tool called
  [`libprotobuf-mutator (LPM)`](https://github.com/google/libprotobuf-mutator)
  which is useful for coverage guided fuzzing. But requires a better
  `xml.proto`, protobuf specification, the one provided in examples is
  simplified and not enough.

- For now I found
  [this](https://git.cs.uni-paderborn.de/xml-signature-mutator/fuzzer)
  project, which is some small university project, but seems promising.
  To have this working we just have to pass the `afl_interface.py` as
  argument to `export AFL_PYTHON_MODULE=/path/to/program`. Before
  starting the fuzzer

## Compiling libvirt with Fuzzers enabled

This part requires a lot of experimentation with the flags but I will go
with what's popular in the oss-fuzz repo.

- Minimal setup:

``` {.sh}
cd ~/libvirt
export CC=afl-clang-fast \
       CXX=afl-clang++-fast \
       GIT_SSL_NO_VERIFY=1
meson setup build \
       -Dsystem=true \
       -Dgit_werror=disabled \
       -Dapparmor=disabled
ninja -C build
```

## Running basic AFL-fuzz

Since libvirt uses `dlopen()` it is recommended in the
[docs](https://github.com/AFLplusplus/AFLplusplus/blob/stable/docs/best_practices.md#fuzzing-a-target-with-dlopen-instrumented-libraries)
to use `LD_LIBRARY_PATH`. But before that we need to make sure the
socket is running in the background.

``` {.sh}
export LD_LIBRARY_PATH="~/libvirt/build/src/" \
       AFL_PRELOAD="$(ls ~/libvirt/build/src/*.so | tr -s '[:space:]' ' ')" 

LD_PRELOAD="$(ls ~/libvirt/build/src/*.so | tr -s '[:space:]' ' ')" ~/libvirt/build/src/virtqemud -d 
LD_PRELOAD="$(ls ~/libvirt/build/src/*.so | tr -s '[:space:]' ' ')" ~/libvirt/build/src/virtnetworkd -d

afl-fuzz -i in/ -o out/ -- ~/libvirt/build/tools/virsh net-create @@
```

## Complications - 1

- `undefined symbol __afl_area_ptr`: I was constantly recieving this
  error when trying to run AFL but after hours of debugging, turns out
  `LD_PRELOAD` should never be exported just passed in like shown above.
  I still get this error for a lot of commands and I'm still working on
  a permanent fix.

- Sockets not running: I was trying to fuzz the binary
  `virsh net-create` but, for virsh to work we need multiple sockets
  running like virtqemud sockets and virtnetworkd sockets. I have a
  `entrypoint.sh` script to run in the background before running the
  `afl-fuzz` command, but for this you need to install libvirt on to
  your system using `sudo ninja -C build install`. After setting this
  entrypoint script i was pretty much able to start fuzzing.

- dnsmasq not found in \$PATH: By default the debian's path is very
  minimal, I had atleast add sbin to it.

``` {.sh}
export PATH=$PATH:/usr/sbin
```

- To fix above, Now to run the fuzzer you can just use the entrypoint
  script, with the following command

``` {.sh}
./init.sh
afl-fuzz -i in/ -o out/ -- ~/libvirt/build/tools/virsh @@
```

- The custom mutator that `AFLPlusPlus` provides in their website, seems
  outdated doesn't have, the method `deinit()` and throws error:

``` {.sh}
AFLPythonmutator module 'wrapper_afl_min' has no attribute 'deinit'. Did you
mean: 'init'?
```

So i had to manually go and make a dummy function which does nothing.

``` {.py}
def deinit():
    pass 
```

## Coverage Reports

Build the coverage source using

``` {.sh}
cd ~/libvirt-cov
export CC=gcc \
       CXX=g++ \
       GIT_SSL_NO_VERIFY=1
meson setup build \
       -Dsystem=true \
       -Dgit_werror=disabled \
       -Dapparmor=disabled \
       -Dtest_coverage=true
ninja -C build
```

- For convinience I setup a `init-cov.sh` to run before running the
  coverage command.

- Here's the command to run covergage:

``` {.sh}
afl-cov -d out/ \
        --code-dir ~/libvirt-cov/build \
        --coverage-cmd "/home/$USER/libvirt-cov/build/tools/virsh net-create @@"
```

- All the coverage reports can be found in the location
  `out-*/cov/web/index.html`.

## Improvements

- Adding a dictionary can make the mutations a lot better. (first
  coverage result happened here)

``` {.sh}
afl-fuzz -x xml.dict -i in/ -o out/ -- ~/libvirt/build/tools/virsh net-create @@
```

- Using a custom XML-aware mutator.

``` {.sh}
export PYTHONPATH="$PWD"
export AFL_PYTHON_MODULE="wrapper_afl_min"
```

**OR**

``` {.sh}
sudo apt install python3-onelogin-saml2 python3-yaml python3-pysaml2
export PYTHONPATH="$PWD/xml_signature_mutator"
export INPUT_DIR="$PWD/in"
export AFL_PYTHON_MODULE="afl_interface"
```

- Using cmplog, just clone and build with AFL complier,

``` {.sh}
git clone https://gitlab.com/libvirt/libvirt libvirt-cmp
cd libvirt-cmp
export CC=afl-clang-fast \
       CXX=afl-clang++-fast \
       GIT_SSL_NO_VERIFY=1
meson setup build \
       -Dsystem=true \
       -Dgit_werror=disabled \
       -Dapparmor=disabled
ninja -C build
```

and change the afl flags to have cmplog enabled.

``` {.sh}
afl-fuzz -x xml.dict -c ~/libvirt-cmp/build/tools/virsh -i in/ -o out/ -- ~/libvirt/build/tools/virsh net-create @@
```

## Conclusion

There are many inherent problems with this approach and the whole idea
of fuzzing the virsh binary is not right:

- Main problem is `virsh` is not really a monolithic binary that handles
  the creation of networks, its just a CLI that parses input to the
  `virtnetworkd` daemon or the `libvirtd` (monolithic). In current setup
  AFL doesn't detect the crashes in the `virtnetworkd` daemon and only
  detect ones from `virsh`, which is only the first layer of parsing
  XML.

- There can be instances where there is a possibility of a bug but its
  not possible to crash it using a single `virsh` command and requires a
  certain configuration to be in place. Hence persistent mode or using
  `LLVMTestOneInput` methods can directly fuzz individual functions
  irrespective of what is happening as a whole.

- The mutators that I spent a lot of time making them work, barely work
  and give any positive results. The `xml.dict` dictionary and `cmplog`
  features have helped a lot though.

- Right now we are just blatantly installing libvirt to the system and
  making the installed sockets LISTEN while `virtqemud` and
  `virtnetworkd` are running in the background. It would be great if we
  just have a single binary as output which we can fuzz.

- I'm just going to archive this attempt, because progress seems to be
  plateaued after using `cmplog`. The coverage is very less and poor
  overall. I will mostly move to AFL's persistent mode which seems to
  have a lot of hype in the fuzzing space.
