# Test fuzzing run with AFL++

This tutorial is documented version of my attempt to perform basic black-box
instrumentation using AFL++ without access to any libvirt source code.

This is used as an introductory challenge before diving into the source code and
using statically linked source code files to fuzz the project effectively.

## Setup

Have a fresh Ubuntu-22.04 setup, preferably on bare-metal.

- Install the latest version of
  [AFLPlusPlus](https://github.com/AFLplusplus/AFLplusplus/blob/stable/docs/INSTALL.md).
  ( I had problems cloning the repo, the solution was to switch to faster
  internet )

- Change some kernel configuration by running these commands as root:

```
# sysctl -w kernel.shmmax=8388608
# sysctl -w kernel.shmall=4096
```

- Install QEMU, used by libvirt to initialize images.

```{.sh}
$ sudo apt install qemu-system
```

- Install libvirt dependencies.

```{.sh}
$ sudo apt-get install \
    git build-essential \
    xsltproc libxml-xpath-perl \
    libyajl-dev libdevmapper-dev libpciaccess-dev libnl-dev systemtap-sdt-dev \
    uuid-dev libtool autoconf pkg-config \
    libxml2 libxml2-utils autopoint \
    libnuma-dev gettext meson wget make libgnutls30 libncurses-dev \
    python3-full python3-docutils \
    black flake8 dwarves augeas-tools
```

- Download and compile libvirt, refer to the [docs](https://libvirt.org/compiling.html).
  Its optional to perform the building and testing in a venv (before compiling).

```{.sh}
$ python3 -m venv venv
$ source ./venv/bin/activate
$ pip install pytest
```

### Known Issues

In an Ubuntu machine, afl might initially throw an error saying `system is
configured to send core dump notifications to an external utility.`.
To fix this just login as root user and disable those notifications.

```
# echo core >/proc/sys/kernel/core_pattern
```

## Fuzzing the application

For the sake of this test, lets just perform a simple binary-only fuzz.

Let's fuzz the `virsh` binary using the `virsh net-create <xml>` command.
I have chosen this command in contrast to the more generic ones like
`virsh define` and `virsh create`, because the size of XML is smaller
and fuzzing will be faster for the sake of this testing.

### Fuzzing with a corpus

- To begin with, copy the `virsh`, the **fuzz target** binary from the system path,
  say `/usr/bin/virsh` into the fuzzing directory. Also create a new folder to
  take input and output from.

```
$ mkdir ~/fuzz
$ cd ~/fuzz
$ mkdir in out
$ cp /usr/bin/virsh .
```

- Now copy the sample input or the **seed corpus** to
  fuzzing directory. In the intial phase I have used a single
  valid XML file and two random XMl files.

- Run the fuzzer using the following command.

```
$ afl-fuzz -Q -D -i in/ -o out/ -- ./virsh net-create @@
```

The additional arguments passed in this command are:

- `-Q`: To use QEMU-mode to emulate the binary
- `-D`: (Optional) To ensure that each test case generated from a particular seed
  input will always produce the same behavior or outcome.

## Improving the seed corpus

Libvirt already has some XML data as a part of unittests that can be fed into
the `virsh net-create` command. I have copied like 71 correct test cases from
the `networkxml2confdata` for libvirt unittests. Meanwhile failing test cases
from various directories like, `bhyveargv2xmldata` and `secretxml2xmlin`, and
are 40 in number.

This is now a better seed corpus with more number of input files, and more
randomness.

## Including a Dictionary

By default the mutations performed by AFL are truly random
and adding a dictionary helps the model explore the grammar behind the
input files, and hugely improve prediction.

I have taken my `xml.dict` file from the [AFL's repo](https://github.com/google/AFL/blob/master/dictionaries/xml.dict).

It can be included in AFL using the `-x` option

```
$ afl-fuzz -Q -D -x xml.dict -i in/ -o out/ -- ./virsh net-create @@
```

## Test results

Well the results, were not that great at all. `virsh` in all seems to be a
really complex and slow binaries. AFL's exec speed never crossed even 15/sec.
One of the reasons as researched could be multiple calls of `exec/execvp` in the
binary which causes serious delays in AFL's runtime.

At first the number of paths explored is as low as 2 for every run,

- The first try was with just 4 XML files in the corpus.

<img title="Bad Corpus Fuzz" alt="dumb" src="/assets/dumb.png">

- My second try was with to improve the corpus to have more XML files, also ran
  it for longer.

<img title="Fuzzing with better input" alt="nodict" src="/assets/qemu_nodict.png">

- Finally I ended up using a dictionary, this time I left it running for really
  long, and enable `-D` option to be deterministic.

<img title="Fuzzing with Dictionary" alt="dict" src="/assets/qemu_dict.png">

## Attempts for Improvement

I have tried various solutions, to improve the execution speed and path
exploration.

- **Check Syntax** - I tried executing some of the test inputs manually using
  the `virsh net-create <xml>` command and they seem to give various kinds of
  outputs just like the unittests, based on file that is passed, so the syntax
  seems to be alright.

- **Experiment testcases** - Maybe the testcases were not great, and give only
  few kinds of output based on input. Example: No matter what the input be,
  it gives the same one or two errors. To combat this I tried switching random
  tags in the valid inputs and passing them, but still see no progress.

- **Persistent Mode** - As mentioned in the AFL documentation to improve speed,
  one of the important suggestions was to use Persistent Mode with `-P` option.
  But this seems to have no affect in qemu-black box mode of AFL.

- **Performance improvements** - I tried using multithread, without multithread
  `-M`, change the Operating system (Opensuse), without display server running,
  etc. But I see no difference.

- **Try other subcommands** - Maybe `virsh create` was too big. Well I tried
  with various virsh sub-commands and ended up going with `virsh net-create`

One of the first things AFL says once application is started is this error

```
The target binary is pretty slow! See fuzzing_in_depth.md#improve-the-speed
```

## Reflection and Conclusion

My conclusion after such experimentation is to simpler binaries unlike `virsh`.
Also if anything, my experience validates the importance of white box testing,
and instrumentation techniques to make more accurate guesses. This offers 
myriad of optimizations like defining of entry points, persistence, in-memory
fuzzing, use of custom mutators etc, but setting up all of that requires some 
expertize.

<img title="Meme to end" alt="meme" src="/assets/meme.png" width="200">
