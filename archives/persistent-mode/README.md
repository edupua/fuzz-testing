# AFL's Persistend Mode

## Introduction

[AFL's
guide](https://github.com/AFLplusplus/AFLplusplus/blob/stable/instrumentation/README.persistent_mode.md)
promises 100x increase in fuzzing speed if you use this method. But
setting it up can be a bit hard.

## Using preeny to help persistent mode fuzzingw

We can use preeny to disable sockets, randomizations, tempfiles, . First
install deps for preeny. This is highly recommended in the AFL docs, so
that the errors are a lot more reporoducible.

``` {.sh}
sudo apt-get install -y libini-config-dev libseccomp-dev
```

Then clone and build preeny:

``` {.sh}
cd ~
git clone https://github.com/zardus/preeny.git
cd preeny
make
```

### Running with preeny

Just export the `LD_PRELOAD` variable to have the shared objects that we
are trying to use in our program. In current setup we might not be using
all of the given tools, its good to have them.

``` {.sh}
export LD_PRELOAD="$HOME/preeny/x86_64-linux-gnu/desock.so $HOME/preeny/x86_64-linux-gnu/desrand.so $HOME/preeny/x86_64-linux-gnu/detime.so"
```

## Running test hypervisor

If we have to fuzz the XML parsing specifically, we can just use the
test hypervisor instead of QEMU. just run the virsh command locally with
the following priviliges.

``` {.sh}
$ virsh -c test:///default net-create in.xml
```

I will be using this instead of using the below method.

## Running virsh as root

Since `qemu:///session` has very less networking priviliges, it is
better to connect to `qemu:///system` instead which requires us to run
virsh as root user. But to do so we need to add some users and groups.

``` {.sh}
# useradd -r libvirt-qemu
# usermod -aG libvirt-qemu libvirt-qemu
# groupadd -r libvirt
# groupadd -r libvirtd
```

After that u can connect using

``` {.sh}
# libvirtd -d
# virsh net-list 
```

## Patches

To fuzz the network create function you need to apply the following
patches listed here.

- **Preeny Desock Patch** - The `preeny-desock.patch` fixes the
  segmentation fault that is being caused. After a lot of debugging I
  ended up getting the solution from [Alt Linux's Fuzzing
  Setup](https://wiki.altlinux.ru/Libvirt_fuzzing_technique)

- **Libvirt Network-create patch** - The `net-create.patch`, add
  instrumentation to the main `net-create` function that the test
  hypervisor uses.

You can apply these patches using:

``` {.sh}
cp patches/preeny-desock.patch ~/preeny
cd ~/preeny
git apply preeny-desock.patch
```

``` {.sh}
cp patches/net-create.patch ~/libvirt
cd ~/libvirt
git apply net-create.patch
```

## Compiling libvirt with fuzzers enabled

Compared to last time, I have disabled optimizations and increased the
stack frame so that debugging is easier.

``` {.sh}
cd ~/libvirt
export CC=afl-clang-fast \
       CXX=afl-clang++-fast \
       CFLAGS="-O0 -Wframe-larger-than=30000" \
       CXXFLAGS="-O0 -Wframe-larger-than=30000" \
       GIT_SSL_NO_VERIFY=1
meson setup build \
       -Dsystem=true \
       -Dgit_werror=disabled \
       -Dapparmor=disabled
ninja -C build
```

## Running basic AFL-fuzz

Only thing that changes here is not requiring any daemon to be running
the background, and the preeny libraries to be loaded.

Also the persistent-mode enusres that the input is taken at the right
instance directly as buffer, so just having a dummy `test.xml` can reach
to that point.

``` {.sh}
export LD_LIBRARY_PATH="~/libvirt/build/src/" \
       AFL_PRELOAD="$(ls ~/libvirt/build/src/*.so | tr -s '[:space:]' ' ') $HOME/preeny/x86_64-linux-gnu/desock.so $HOME/preeny/x86_64-linux-gnu/detime.so $HOME/preeny/x86_64-linux-gnu/desrand.so" 

afl-fuzz -i in/ -o out/ -- ~/libvirt/build/tools/virsh -c test:///default net-create test.xml
```

## Gathering coverage

For coverage reports we will freshly clone and build libvirt into
another directory.

``` {.sh}
cd ~
git clone https://gitlab.com/libvirt/libvirt.git libvirt-cov
cd ~/libvirt-cov
export CC=gcc \
       CXX=g++ \
       CFLAGS="-Wno-error -O0 -Wframe-larger-than=30000" \
       CXXFLAGS="-Wno-error -O0 -Wframe-larger-than=30000" \
       GIT_SSL_NO_VERIFY=1
meson setup build \
       -Dsystem=true \
       -Dgit_werror=disabled \
       -Dapparmor=disabled \
       -Dtest_coverage=true
ninja -C build
```

Now we define the target to take file input and

``` {.sh}
export LD_LIBRARY_PATH="~/libvirt-cov/build/src/" \
       AFL_PRELOAD="$(ls ~/libvirt-cov/build/src/*.so | tr -s '[:space:]' ' ') $HOME/preeny/x86_64-linux-gnu/desock.so $HOME/preeny/x86_64-linux-gnu/detime.so $HOME/preeny/x86_64-linux-gnu/desrand.so" 

afl-cov -d out/ \
        --code-dir ~/libvirt-cov/build \
        --coverage-cmd "$HOME/libvirt-cov/build/tools/virsh -c test:///default net-create @@"
```

## Corpus setup

The test corpus that i have taken has a common problem. The network name
`default` already exists in test hypervisor by default. So it may give
an error saying

``` {.sh}
error: operation failed: network 'default' already exists with uuid
dd8fe884-6c02-601e-7551-cca97df1c5df
```

To fix this i have replaced all `default` with `default0`

``` {.sh}
sed -i 's/<name>default<\/name>/<name>default0<\/name>/g' in/*.xml
```

## Optimizations

- Using dictionary. This is trivial and a must, based on past results.

``` {.sh}
afl-fuzz -x xml.dict -i in/ -o out/ -- ~/libvirt/build/tools/virsh -c test:///default net-create test.xml
```

- Will use one of the python mutator from last time, prefferably the one
  example mutator provided by AFL.

``` {.sh}
export PYTHONPATH="$PWD"
export AFL_PYTHON_MODULE="wrapper_afl_min"
```

This mutator has the `deinit()` fix. Which basically adds an empty
function, which fixes the error caused. I even verified it with Nicolas
Grégoire; the person who wrote this mutator.

``` {.py}
def deinit():
   pass 
```

- Using cmplog feature - This was very beneficial last time so I will
  try this time too. This basically requires you to compile yet another
  instance of libvirt. Instructions are same as last time except the
  `libvirt-cmp` shud also have the `net-create.patch` applied.

This feature is more often known by its fancy name; *Redqueen mutator*.
Turning it on requires us to export a variable called `AFL_LLVM_CMPLOG`.

``` {.sh}
cd ~
git clone https://gitlab.com/libvirt/libvirt libvirt-cmp
cd libvirt-cmp
cp ~/libvirt/net-create.patch .
git apply net-create.patch
export CC=afl-clang-fast \
       CXX=afl-clang++-fast \
       CFLAGS="-Wno-error -O0 -Wframe-larger-than=30000" \
       CXXFLAGS="-Wno-error -O0 -Wframe-larger-than=30000" \
       GIT_SSL_NO_VERIFY=1 \
       AFL_LLVM_CMPLOG=1
meson setup build \
       -Dsystem=true \
       -Dgit_werror=disabled \
       -Dapparmor=disabled
ninja -C build
```

Now use it:

``` {.sh}
afl-fuzz -x xml.dict \
         -c ~/libvirt-cmp/build/tools/virsh \
         -i in/ -o out/ \ 
         -- ~/libvirt/build/tools/virsh -c test:///default net-create test.xml
```

There are many other configuration values that can be passed to cmplog,
we will use it once we see better results.

- Using Mopt mutator. Using it is pretty simple just add `-L -1`

``` {.sh}
afl-fuzz -x xml.dict \
         -L -1 \
         -i in/ -o out/ \ 
         -- ~/libvirt/build/tools/virsh -c test:///default net-create test.xml
```

- Experiment with power `-p` option, this has many different power
  schedules, we will start seeing major differences only when we run it
  for several hours. Here's the
  [Doc](https://aflplus.plus/docs/power_schedules/)

## Major Optimizations

These changes require recompiling libvirt, or even sometimes recompiling
AFLPlusPlus.

- Using sanitizers. We can use `ASAN+UBSAN` at once, but others need to
  be used indepentendly. For now these flags need to be added before
  compiling libvirt. I will additionally use MSAN and TSAN seperately.

  - In AFL we just need to export a variable to activate sanitizers.

  ``` {.sh}
  export AFL_USE_ASAN=1 \
         AFL_USE_UBSAN=1

  export AFL_USE_MSAN=1

  export AFL_USE_TSAN=1
  ```

  - When using ASAN we have to add `-m none` option, but that means AFL
    won't limit the memory at all and potentially crash the system. I
    will put a memory limit of 1GB, `-m 1024`.

  - We can also use hardened mode (not with msan), `AFL_HARDEN=1` (this
    doesn't work with lto)

  ``` {.sh}
  export AFL_HARDEN=1
  ```

- Using `afl-clang-lto` instead of `afl-clang-fast` seem to have some
  good benefits. This also requires us to set some other varibles like
  `AR`, `RANLIB`.

``` {.sh}
export CC=afl-clang-lto \
       CXX=afl-clang-lto++ \
       CFLAGS="-O0 -Wframe-larger-than=30000" \
       CXXFLAGS="-O0 -Wframe-larger-than=30000" \
       AR=llvm-ar \
       RANLIB=llvm-ranlib \
       AS=llvm-as 
```

- Using NGRAM coverage. This might not promise improving performance but
  worth a shot. It requires us to recompile `AFLPlusPlus` because we are
  changing the variable `MAP_SIZE_POW2`. (i have created a patch file
  for it)

  ``` {.sh}
  cp patches/map-size.patch ~/AFLPlusPlus
  git apply map-size.patch
  ... (rest of the build steps)
  ```

  - The change needs to be made in `include/config.h`. Change the value
    from 16 to 18. Then compile `AFLPlusPlus` again.
  - Now we can just set the environment variable
    `AFL_LLVM_INSTRUMENT=NGRAM-{value}`, i prefer value 4.

There are different instrumentation modes out there, i will try
assessing the performance of `CFG (lto)`, `NGRAM-4`, and `CTX`

``` {.sh}
export AFL_LLVM_INSTRUMENT=CTX
export AFL_LLVM_INSTRUMENT=NGRAM-4
```

## Multithreading

Basically if you have multiple cores, you can have multiple instances of
AFL running, each with its own minor configurational change. Recommended
number is about 32 instances running.

The point is the input corpus and the output directory is same between
all these instances of AFL, but the "way" of fuzzing changes.

This is how I will be running, whenever I have access to a multicore
machine.

Doing so is simple, just have a main fuzzer running with

``` {.sh}
./afl-fuzz -i testcase_dir -o sync_dir -M main [...other stuff...]
```

And a bunch of other secondary fuzzers named according to their
configurational names.

``` {.sh}
./afl-fuzz -i testcase_dir -o sync_dir -S lto [...other stuff...]
./afl-fuzz -i testcase_dir -o sync_dir -S ngram [...other stuff...]
```

### Limitations

- The `AFL_LOOP()` feature is not working as of right now, that's why i
  did not pass a number to it like 1000 is the default. Right now only 1
  iteration occurs at each fork by AFL. This is because the state is not
  being reset as intended and there is so much multithreading involved
  that AFL cannot trace down it original fork. Having this running could
  increase performance by like 10x.

- The current coverage doesn't change much and tanks after like 1 hour
  of run, no matter how many other optimizations you use.

## Future Planning

I plan on making a seperate harness, which is a custom `.c` file which
fuzzes the function directly instead of trying to use virsh or test
hypervisor. (even though we need to pass a test hypervisor to the
function anyway).

I plan on trying to replicate the testing procedure in
`tests/objecteventtest.c`. This unittest doesn't involve much
multithreading and forking, and I will remove all instances of timeouts
and randomization to make sure AFL's persistent mode doesn't interfere.

**Note**: And current configuration works good enough to maybe find some
bugs. I still have some instances running since days.
