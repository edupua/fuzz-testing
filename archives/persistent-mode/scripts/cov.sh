#!/bin/bash

if [[ $# -lt 1 ]]; then
   echo "Usage: ./cov.sh <out-dir>"
   exit
fi

export LD_LIBRARY_PATH="~/libvirt-cov/build/src/" \
       AFL_PRELOAD="$(ls ~/libvirt-cov/build/src/*.so | tr -s '[:space:]' ' ') $HOME/preeny/x86_64-linux-gnu/desock.so $HOME/preeny/x86_64-linux-gnu/detime.so $HOME/preeny/x86_64-linux-gnu/desrand.so" 

afl-cov -d $1 \
        --code-dir ~/libvirt-cov/build \
        --coverage-cmd "$HOME/libvirt-cov/build/tools/virsh -c test:///default net-create @@"

