#!/bin/bash

if [[ $# -lt 1 ]]; then
    echo "Usage: ./<script>.sh <num-instances>"
    exit 
fi

export LD_LIBRARY_PATH="~/libvirt/build/src/" \
       PYTHONPATH="$PWD" \
       AFL_PYTHON_MODULE="wrapper_afl_min" \
       AFL_PRELOAD="$(ls ~/libvirt/build/src/*.so | tr -s '[:space:]' ' ') $HOME/preeny/x86_64-linux-gnu/desock.so $HOME/preeny/x86_64-linux-gnu/detime.so $HOME/preeny/x86_64-linux-gnu/desrand.so" 

afl-fuzz -x xml.dict -i in/ -o out/ -M main -- ~/libvirt/build/tools/virsh -c test:///default net-create test.xml

for i in {1..$1}; do
    afl-fuzz -x xml.dict -i in/ -o out/ -S sec$i -- ~/libvirt/build/tools/virsh -c test:///default net-create test.xml
done
