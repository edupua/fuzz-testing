export LD_LIBRARY_PATH="~/libvirt/build/src/" \
       AFL_PRELOAD="$(ls ~/libvirt/build/src/*.so | tr -s '[:space:]' ' ') $HOME/preeny/x86_64-linux-gnu/desock.so $HOME/preeny/x86_64-linux-gnu/detime.so $HOME/preeny/x86_64-linux-gnu/desrand.so" 

afl-fuzz -x xml.dict \
         -c ~/libvirt-cmp/build/tools/virsh \
	 -L -1 \
         -i in/ -o out/ -- ~/libvirt/build/tools/virsh -c test:///default net-create test.xml

