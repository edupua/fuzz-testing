# Using a Harness with AFL Persistent mode

Changing the source code of virsh related binaries includes a lot of
variables to be taken account of, including having to deal with
multithreading and error-handling

Its better to introduct new simple `.c` files called harnesses which
have a single main function and they initialize and call other
functions, kinda like unittests. (most of the code and build options are
also insipred by existing unittests).

We don't need preeny in such a setup, thus removed all dependent traces.

## Build

This time I have made seperate build scripts for convinience cause it
requires us to build libvirt multiple times parallelly.

The naming convention for the libvirt source directories is self
explanatory:

``` {.sh}
~/libvirt    # has vanilla libvirt which will be compiled by afl-clang-fast
~/libvirt-cov   # built with gcc, g++, used for coverage measurement and has different patch files
~/libvirt-cmp   # built with cmplog feature (redqueen)
~/libvirt-lto   # built with afl-clang-lto to build
~/libvirt-lto-cmp   # built with redqueen with lto
~/libvirt-asan-ubsan  # build with address sanitizer and undefined behaviour sanitizer
~/libvirt-msan  # build with memory sanitizer
... ( and so on )
```

- **Update**: Currently we have changed the build system to have
  different build directories instead of different libvirt clones. Just
  like the clones the build directories are named as `build_cov`,
  `build_lto` etc. I have made a really detailed `build.sh` script which
  can be used to handle this.

## Patches

Right now there are two patches for libvirt, one for introducing a
harnesses for various commands meant for afl, and other without
persistent loops and gcc in mind (coverage).

The patch for coverage collection is different and named
`harness-cov.patch` since it uses `gcc` compiler and doesn't recognize
AFL syntax like `AFL_INIT`. So the harness is rewritten to do the same
thing as the instrumented target. This is meant to be applied to the
`libvirt-cov` clone of libvirt.

The patch for AFLPlusPlus is `map-size.patch`, which increases the
map-size useful for mutators like NGRAM.

- **Update** - One way to test the fuzzers is to introduce some known
  crashes in the program which can lead to all sorts of errors. To start
  with I've introduced 3 such errors meant for `network-create`.

  - Error 1 - If the mutator decides to change the length of the sizee
    attribute of `<mtu>`, this should get triggered and NULL pointer
    assignment should break the program.
  - Error 2 - Having a `<virtualport>` element in the root and also
    having a `<portgroup>`, should trigger this and lead to segmentation
    fault.
  - Error 3 - If nRoutes in ForwardNat is greater than 3 then, then
    NetDevIPAddr will get allocated instead of IPRoute, which should
    lead to null pointer dereference, or similar errors.

- **Update 2** - I stopped using patches for changing libvirt, but still
  use it for small changes in AFLPlusPlus. Right now I have a
  [fork](https://gitlab.com/edupua/libvirt) of libvirt which has
  different branches for different set of patches applied.

## Scripts

There is a script directory consisting of all combinations of `afl-fuzz`
commands with slight variations in them. I will keep adding to these, as
I explore.

There is a `test.sh` which is used to perform basic sanity check to test
out the current harness binary and its working. Often use gdb in a
combination with this.

There is a `cov.sh` script which can generate coverage for a given `out`
directory.

- **Update**: I have added support for differen commands other than
  `net-create`, the scripts now take an argument, Eg; `vol-create`,
  `nwfilter-define`, which are fragile hardcoded names to be followed
  for inputs directories `in-net-create` as well as sample xml files
  `net-create-test.xml` (used by `test.sh`) and are originally based on
  the harness names `net-create-harness`.

A lot of scripts have untold prequisites I will try to document as I
feel the need to do so.

- `lto` related scripts are to be run only when `build-lto` exists
  (compiled with `afl-clang-lto`)

- `redqueen` related scripts require the `build-cmp`, compiled with
  `CMPLOG` flag enabled.

- **Update 2:** There are two new big scripts that handle almost every
  possibility of options themselves.

  - The `base.sh` requires two arguments, the name of the command and
    the name of the build directory. If cmplog is a part of the name of
    the build dir, then it automatically finds the non-cmplogged version
    and runs the fuzzer. This script is a generalization of the old
    `dict.sh`

  - The 'gen_tmux.sh' is used to make tmux commands with a variety of
    combinations of options. This command should generate a `mega.sh`
    script file with the required tmux commands, and should be ran when
    ready to fuzz. First thing it does is try to find as many build
    directories as possible in the `~/libvirt`, and apply the following
    option assignment logic:

    - -p \<fast, explore, rare\>: Each of the above compiler should have
      these 3 options set individually in 2:1:1 ratio, means if there
      are 10 instances, after this there must be 20:10:10 instances with
      fast option more frequent.

    - -P \<explore, exploit\>: assign these to any 20 of the 40
      instances.

    - -a \<binary, text\>: Same assign these to any 20 of the 40
      instances.

    - randomly -Z: for like 5-6 of the 40 have this option.

  - There are other scripts too, which are useful when testing out, or
    performing sanity checks on the harnesses. Along with the old
    `test.sh`, we have a `debug.sh`, which runs a basic instance of
    afl-fuzz with `AFL_DEBUG` option set.

## Progress

The exec speed for the first time is in 1000s, and the coverage is
really good, I think enabling enough sanitizers, we should be able to
find bugs once we use a stronger machine for longer hours.

- **Update**: I just ran a 24-hour fuzzing run with only the
  custom-python mutator `xml-signature-fuzzer` to check how good it
  performs, and the results are underwhelming. First problem is the
  `no. of catches/no. of tries` metric for the `py` section in the
  fuzzing strategy yields section shows `0/0`. That should mean that the
  mutator has not given any useful input. But when tried along with
  `AFL_CUSTOM_MUTATOR_ONLY` and `AFL_DISABLE_TRIM`, the value still
  shows `0/0`. Which is just wrong, because I see some new corpuses
  begin generated and see the `findings in depth` metric go up.

  - After analysing the corpus I have noticed that most of the inputs
    were wasteful and like 300+ of the mutations were performed on a
    single input file called `openvswitch-net`, and those mutations were
    just removing deleting a few lines (whole lines), or just
    duplicating them. Either way this is not a sustainable fuzzing. We
    need more minimalist corpuses to target better.

  - The main problem here is not just the corpus but the mutator itself.
    Originally the python mutator promises making changes to the XML
    elements. But apart from deleting lines, it has done nothing
    special. If anything we are in quick need of a really good mutator
    that can actually switch parameters.

- **Update 2:** The current fuzzer is really struggling after running
  for 18 hrs or so. Most of the mutatons are just useless bit-flips, we
  really need to bring in Libprotobuf or any other structure aware
  mutator.

- The new scripts will definitely make it easier for us to run the
  fuzzers but there are several techniques like Mopt and other power
  schedules, which i have not used because they are not that important
  and barely show any difference.

  - Mopt or any other mutator scheduling policy is meant for choosing
    better mutators but right now we are aiming to construct a single
    big mutator that won't require any scheduling.
  - The different power schedules according to the
    [docs](https://aflplus.plus/docs/power_schedules/), are just slight
    variations of the `-p fast`, so i have included only `rare` and
    `explore` based on popularity.
