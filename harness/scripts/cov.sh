#!/bin/bash

if [[ $# -lt 1 ]]; then
   echo "Usage: ./cov.sh <command>"
   echo "<command> can have the following values: [net-create, net-port-create, nwfilter-define, vol-create, pool-create, domain-create]"
   exit
fi

export LD_LIBRARY_PATH="~/libvirt/build_cov/src/"

afl-cov -d outputs/out-$1 \
        --code-dir ~/libvirt/build_cov \
        --coverage-cmd "$HOME/libvirt/build_cov/fuzz/$1-harness"
