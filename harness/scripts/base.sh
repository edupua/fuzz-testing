#!/bin/bash

if [[ $# -lt 2 ]]; then
   echo "Usage: ./script.sh <command> <build-dir>"
   echo "<command> can have the following values: [net-create, net-port-create, nwfilter-define, vol-create, pool-create]"
   echo "<build_dir> must be an existing directory where libvirt was compiled, suffixed with options, Eg: build_lto, build_cov"
   echo "Make sure you have input directories corresponding to the <command>, Eg: in-net-create, in-nwfilter-define"
   exit
fi


COMMAND=$1
BUILD_DIR=$2

# Check if command is valid
valid_commands=("net-create" "net-port-create" "nwfilter-define" "vol-create" "pool-create", "domain-create")
if [[ ! " ${valid_commands[@]} " =~ " ${COMMAND} " ]]; then
    echo "Error: Invalid command. Valid commands are: ${valid_commands[*]}"
    exit 1
fi

# Check if build dir exists
if [ ! -d "$HOME/libvirt/$BUILD_DIR" ]; then
    echo "Error: Directory $BUILD_DIR does not exist in libvirt dir."
    exit 1
fi


CMP_LOG=""
shift 2

export LD_LIBRARY_PATH="~/libvirt/$BUILD_DIR/src/" \
       AFL_PRELOAD="$(ls ~/libvirt/$BUILD_DIR/src/*.so | tr -s '[:space:]' ' ')"

if [[ $BUILD_DIR =~ "_cmplog" ]]; then
    CMP_LOG="-c $HOME/libvirt/$BUILD_DIR/fuzz/$COMMAND-harness"
    BUILD_DIR=${BUILD_DIR%_cmplog}
fi

afl-fuzz \
    -i corpus/in-$COMMAND \
    $CMP_LOG \
    $@ \
    -o outputs/out-$COMMAND -- ~/libvirt/$BUILD_DIR/fuzz/$COMMAND-harness
