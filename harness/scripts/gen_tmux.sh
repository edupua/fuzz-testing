#!/bin/bash

set -e
if [[ $# -lt 1 ]]; then
    echo "Usage: $0 <command> [--custom_mutator]"
    echo "<command> can have the following values: [net-create, net-port-create, nwfilter-define, vol-create, pool-create, domain-create]"
    echo "Using --custom_mutator argument will disable the default AFLplusplus mutators and use the custom python mutator"
    exit 1
fi

# check if command if valid
COMMAND=$1
valid_commands=("net-create" "net-port-create" "nwfilter-define" "vol-create" "pool-create")
if [[ ! " ${valid_commands[@]} " =~ " ${COMMAND} " ]]; then
    echo "Error: Invalid command. Valid commands are: ${valid_commands[*]}"
    exit 1
fi

# check custom mutator
EXEC_SCRIPT="scripts/base.sh"
if [[ $2 == "--custom_mutator" ]]; then
    EXEC_SCRIPT="scripts/custom_mutator.sh"
fi

if [[ ! -f $EXEC_SCRIPT ]]; then
    echo "$EXEC_SCRIPT: not found, need the base script to run tmux"
    exit 1
fi


# look at the build script to get the build directory naming scheme
get_build_dir() {
    local compiler=$1
    local sanitizer=$2
    local cmplog=$3

    local BUILD_DIR="build_${compiler}"
    if [ "$sanitizer" != "default" ]; then
        BUILD_DIR="${BUILD_DIR}_${sanitizer}"
    fi
    if [ "$cmplog" = true ]; then
        BUILD_DIR="${BUILD_DIR}_cmplog"
    fi

    [ -d "$HOME/libvirt/$BUILD_DIR" ] && echo "$BUILD_DIR" || echo ""
}

generate_fuzz_command() {
    local build_dir=$1
    local index=$2
    local total_instances=$3

    # Add AFL_DISABLE_TRIM for the last 30% of instances
    if [ $index -gt $((total_instances * 70 / 100)) ]; then
        local cmd="AFL_DISABLE_TRIM=1 "
    else
        local cmd=""
    fi

    cmd="${cmd}$EXEC_SCRIPT $COMMAND $build_dir -S slave$index -b $index"

    # Assign -p option in (1 1 2) weights
    if [ $((index % 4)) -eq 0 ]; then
        cmd="$cmd -p rare"
    elif [ $((index % 4)) -eq 1 ]; then
        cmd="$cmd -p explore"
    else
        cmd="$cmd -p fast"
    fi

    # Assign -P option to last half of instances
    if [ $index -gt $((total_instances / 2)) ]; then
        if [ $((index % 2)) -eq 0 ]; then
            cmd="$cmd -P explore"
        else
            cmd="$cmd -P exploit"
        fi
    fi

    # Assign -a option to last half of instances
    if [ $index -gt $((total_instances / 2)) ]; then
        if [ $((index % 2)) -eq 0 ]; then
            cmd="$cmd -a binary"
        else
            cmd="$cmd -a text"
        fi
    fi

    # Assign -Z to last 15% of instances
    if [ $index -gt $((total_instances * 85 / 100)) ]; then
        cmd="$cmd -Z"
    fi

    echo "$cmd"
}

compilers=("default" "lto")
sanitizers=("default" "asan" "ubsan" "tsan" "harden")
cmplog_options=(true false)

build_dirs=()

# Generate all possible build directories
for compiler in "${compilers[@]}"; do
    for sanitizer in "${sanitizers[@]}"; do
        for cmplog in "${cmplog_options[@]}"; do
            build_dir=$(get_build_dir "$compiler" "$sanitizer" "$cmplog")
            if [ -n "$build_dir" ]; then
                build_dirs+=("$build_dir")
            fi
        done
    done
done

total_instances=$((${#build_dirs[@]} * 4))

# print all the build dirs for info
printf "\n"
echo -e "\033[1mBuild Directories: ${#build_dirs[@]}\033[0m"
echo -n -e "\033[1m"
printf "%s, " "${build_dirs[@]}" | sed 's/, $//'
echo -e "\033[0m"

# Generate tmux commands
exec > scripts/mega.sh
echo "tmux new-session -s fuzz '$EXEC_SCRIPT $COMMAND ${build_dirs[0]} -M master -b 0' ';' \\"
echo "     rename-window master ';' \\"

for i in $(seq 1 $total_instances); do
    build_dir=${build_dirs[((i-1) / 4)]}
    fuzz_cmd=$(generate_fuzz_command "$build_dir" "$i" "$total_instances")
    echo "     new-window -n slave$i bash -c 'sleep 5; $fuzz_cmd' ';' \\"
done

echo "     select-window -t master"
exec > /dev/tty
chmod +x scripts/mega.sh
