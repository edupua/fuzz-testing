#!/bin/bash

BUILD_DIR="build"
COMPILER="default"
CMPLOG=false
SANITIZER="default"
COV=false
ALL=false
COMMON_FLAGS="
    -Dsystem=true
    -Dfuzz_tests=enabled
    -Dgit_werror=disabled
    -Dapparmor=disabled
    -Ddocs=disabled
    -Db_lundef=false
    -Ddriver_openvz=disabled
    -Ddriver_qemu=disabled
    -Ddriver_vbox=disabled
    -Ddriver_vmware=disabled
    -Ddriver_libxl=disabled
    -Ddriver_lxc=disabled
    -Ddriver_ch=disabled
    -Ddriver_esx=disabled
    -Dselinux=disabled"

# check if libvirt is cloned in the right location (so that other scripts work)
if [ ! -d "$HOME/libvirt" ]; then
    echo "E: Libvirt should be cloned at the root of the home directory"
    exit 1
elif [ $(basename "$PWD") != "libvirt" ]; then
    echo "W: Not in libvirt directory, moving to libvirt directory at ~/libvirt"
    cd ~/libvirt
fi

print_usage() {
    echo "Usage: $0 [--help] [--cov] [--all] [--compiler <default/lto>] [--cmplog] [--sanitizer <default,asan,ubsan,tsan,harden>]"
    echo "Options:"
    echo "  --help                  Show this help message"
    echo "  --cov                   Build for coverage (incompatible with other options)"
    echo "  --all                   Build all combinations (except coverage)"
    echo "  --compiler <type>       Specify compiler type (default or lto)"
    echo "  --cmplog                Enable cmplog feature"
    echo "  --sanitizer <type>      Specify sanitizer type"
    echo \n "The 'compiler' defaults to 'afl-clang-fast' when nothing or default is specified"
    echo "The 'sanitizer' defaults to 'asan+ubsan' when nothing or default is specified"
}

# parse args
while [[ $# -gt 0 ]]; do
    case $1 in
        --help)
            print_usage
            exit 0
            ;;
        --cov)
            COV=true
            shift
            ;;
        --all)
            ALL=true
            shift
            ;;
        --compiler)
            if [[ $2 != "default" && $2 != "lto" ]]; then
                echo "Error: Invalid compiler type. Use 'default' or 'lto'."
                print_usage
                exit 1
            fi
            COMPILER=$2
            shift 2
            ;;
        --cmplog)
            CMPLOG=true
            shift
            ;;
        --sanitizer)
            if [[ $2 != "default" && $2 != "asan" && $2 != "ubsan" && $2 != "tsan" && $2 != "harden" ]]; then
                echo "Error: Invalid sanitizer type."
                print_usage
                exit 1
            fi
            SANITIZER=$2
            shift 2
            ;;
        *)
            echo "Error: Unknown option $1"
            print_usage
            exit 1
            ;;
    esac
done

unset_all() {
    unset CC CXX AR RANLIB AS CFLAGS CXXFLAGS
    unset AFL_LLVM_CMPLOG AFL_HARDEN GIT_SSL_NO_VERIFY
}


# single build operation
do_build() {
    local compiler=$1
    local cmplog=$2
    local sanitizer=$3

    BUILD_DIR="build_${compiler}"
    if [ "$sanitizer" != "default" ]; then
        BUILD_DIR="${BUILD_DIR}_${sanitizer}"
    fi
    if $cmplog; then
        BUILD_DIR="${BUILD_DIR}_cmplog"
    fi

    COMPILER_FLAGS="-O0 -Wframe-larger-than=30000"

    if [ "$compiler" == "lto" ]; then
        export CC="afl-clang-lto"
        export CXX="afl-clang-lto++"
        export AR="llvm-ar"
        export RANLIB="llvm-ranlib"
        export AS="llvm-as"
    else
        export CC="afl-clang-fast"
        export CXX="afl-clang-fast++"
    fi
    export CFLAGS="$COMPILER_FLAGS"
    export CXXFLAGS="$COMPILER_FLAGS"
    export GIT_SSL_NO_VERIFY=1

    SANITIZER_FLAGS=""
    case $sanitizer in
        default)
            SANITIZER_FLAGS="-Db_lundef=false -Db_sanitize=address,undefined"
            ;;
        asan)
            SANITIZER_FLAGS="-Db_sanitize=address"
            ;;
        ubsan)
            SANITIZER_FLAGS="-Db_lundef=false -Db_sanitize=undefined"
            ;;
        tsan)
            SANITIZER_FLAGS="-Db_sanitize=thread"
            ;;
        harden)
            if [ "$compiler" == "lto" ]; then
                echo "Skipping: AFL_HARDEN is not compatible with LTO compiler."
                return
            fi
            export AFL_HARDEN=1
            ;;
    esac

    if $cmplog; then
        export AFL_LLVM_CMPLOG=1
    fi

    echo "Building with: compiler=$compiler, cmplog=$cmplog, sanitizer=$sanitizer"
    meson setup $BUILD_DIR \
        $COMMON_FLAGS \
        $SANITIZER_FLAGS

    ninja -C $BUILD_DIR
    printf "\n\n"
    echo -e "\033[1mBuild completed in directory: $BUILD_DIR\033[0m"
    echo -e "\033[1m----------------------------------------\033[0m"
    printf "\n\n"

    unset_all
}

# Function to build all combinations
build_all() {
    local compilers=("default" "lto")
    local cmplog_options=(false true)
    local sanitizers=("default" "tsan" "harden")

    for compiler in "${compilers[@]}"; do
        for cmplog in "${cmplog_options[@]}"; do
            for sanitizer in "${sanitizers[@]}"; do
                do_build "$compiler" "$cmplog" "$sanitizer"
            done
        done
    done
}

# Execute build
if $COV; then
    if $ALL; then
        echo "Error: Coverage build cannot be combined with --all option."
        exit 1
    fi
    BUILD_DIR="build_cov"
    export CC="gcc"
    export CXX="g++"
    export CFLAGS="-Wframe-larger-than=124064"
    export CXXFLAGS="-Wframe-larger-than=124064"
    export GIT_SSL_NO_VERIFY=1
    meson setup $BUILD_DIR \
        -Dtest_coverage=true \
	-Dfuzz_coverage=enabled \
        $COMMON_FLAGS
    ninja -C $BUILD_DIR
    echo "Coverage build completed in directory: $BUILD_DIR"
    unset_all
elif $ALL; then
    build_all
else
    do_build "$COMPILER" "$CMPLOG" "$SANITIZER"
fi
