# LLVMFuzzerTestOneInput

## Introduction

Since we aim to have `oss-fuzz` integration, this is what we have to resort to
do. AFLPlusPlus has
[support](https://github.com/AFLplusplus/AFLplusplus/blob/stable/docs/fuzzing_in_depth.md#g-libfuzzer-fuzzer-harnesses-with-llvmfuzzertestoneinput) 
for libfuzzer-style functions and also has support
for advanced features like `FuzzedDataProvider` and `LLVMFuzzerInitialize()`,
and has AFL features like persistent mode and shared-memory test-cases built
into them.
